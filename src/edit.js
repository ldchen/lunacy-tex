
import BrowserWindow from "sketch-module-web-view"
import { getWebview } from "sketch-module-web-view/remote"
import UI from "sketch/ui"

import sketch from "sketch"
import Settings from "sketch/settings"

import { Rectangle, Artboard } from "sketch/dom"


const webviewIdentifier = "tex.webview"

export default function () {
  const selectedDocument = sketch.getSelectedDocument()
  if (selectedDocument.selectedLayers.length < 1){
    UI.message("LaTeX ⚡️ Sketch: Please select an element");
    return;
  }
  // UI.message("Pass")
  const origin_eq = selectedDocument.selectedLayers.layers[0]
  // console.log(origin_eq)
  UI.message("Tex: Editing "+ origin_eq.name)
  const tex_string = Settings.layerSettingForKey(origin_eq, 'tex-string')
  console.log(!origin_eq)
  if(!origin_eq || typeof tex_string == 'undefined') {
    UI.message("LaTeX ⚡️ Sketch: Please select an equation");
    return;
  }
  const origin_x = origin_eq.frame.x
  const origin_y = origin_eq.frame.y

  // console.log("To ASCII")
  // console.log(container)
  // Setup browser
  //======================
  const options = {
    identifier: webviewIdentifier,
    width: 400,
    height: 500,
    show: true,
  }
  const browserWindow = new BrowserWindow(options)

  // // only show the window when the page has loaded to avoid a white flash
  browserWindow.once("ready-to-show", () => {
    browserWindow.show()
  })
  const webContents = browserWindow.webContents

  // const length = origin_eq.name.length
  // // print a message when the page loads
  webContents.on("did-finish-load", () => {
    webContents.executeJavaScript(`updatePreview('${tex_string}')`)
  })
  browserWindow.loadURL(require("../resources/webview.html"))
  browserWindow.webContents.on('svgUpdate', 
  function(svgString) {
    const group = sketch.createLayerFromData(svgString[0], "svg")
    const ngroup = group.layers[1]
    const ratio = origin_eq.frame.height/ngroup.frame.height
    const newFrame = new Rectangle(
      origin_x, origin_y, ratio*ngroup.frame.width, ratio*ngroup.frame.height
    )    
    ngroup.frame = newFrame
    origin_eq.name = '$' + svgString[1]
    const tex_string = svgString[1]
    const container = []
    for (var i = 0; i < tex_string.length; i++) {
      container.push(tex_string[i].charCodeAt(0));
    }
    Settings.setLayerSettingForKey(origin_eq, 'tex-string', container)
    origin_eq.layers = ngroup.layers
    origin_eq.adjustToFit()
    // origin_eq.frame = newFrame
    origin_eq.frame.x = origin_x
    origin_eq.frame.y = origin_y
    // console.log(origin_eq.frame.x, origin_eq.frame.y)
    origin_eq.selected = true
    UI.message("Tex: "+origin_eq.name+" is updated")
    browserWindow.close()
  })
}

// When the plugin is shutdown by Sketch (for example when the user disable the plugin)
// we need to close the webview if it's open
export function onShutdown() {
  const existingWebview = getWebview(webviewIdentifier)
  if (existingWebview) {
    existingWebview.close()
  }
}
