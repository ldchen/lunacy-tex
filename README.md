# Lunacy-Tex

Allow inserting and editing Tex equation as svg in Lunacy.  
This plugin is a quick hack from "https://github.com/heytitle/latex-sketch-plugin"
The plugin contains a webview UI to edit text and load SVG from Mathjax.  

## Installation

1. Download `lunacy-tex.sketchplugin` from release page.
2. Import the plugin folder in Lunacy

![](demo/install.gif)

## Usage

Invoke the Insert/Edit window from toolbar(or using #Keyboard-shortcuts).  
Writting equation in tex math format.

![](demo/usage.gif)


## Keyboard shortcuts:

- [ctrl + shift + l]: Insert Equation
  - An object should be selected
    - If Arttable is selected, the equation will be inserted at upper left corner of the selected Arttable.
    - If other types of layer is selected, the equation will be inserted at 100 unit below the selected layer.
- [ctrl + shift + e]: Edit Equation
  - An object with setting (`tex-string`) should be selected.


## Important technical details

The equation is stored in plain text in group's name(`${plain equation text}`) and in setting property `tex-string`.  
The property can be get/set through Sketch API `Settings.layerSettingForKey`/`Settings.setLayerSettingForKey()`.  
To avoid complex escape character in javascript communication(*i.e.* I don't know how to handle this situation.), the tex equation are converted to ASCII integer array for storing.


## Develop

CLI commands
``` sh
# install dependency
npm install

# build with hot reload
npm run watch

# build for production
npm run build

# for publishing
skpm publish minor
```




