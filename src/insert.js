
import BrowserWindow from "sketch-module-web-view"
import { getWebview } from "sketch-module-web-view/remote"
import UI from "sketch/ui"

import sketch from "sketch"
import Settings from "sketch/settings"
import { Rectangle, Artboard } from "sketch/dom"


const webviewIdentifier = "tex.webview"

export default function () {
  const selectedDocument = sketch.getSelectedDocument()
  if (selectedDocument.selectedLayers.layers.length < 1){
    UI.message("Tex: Please select an object");
    return;
  }
  var myArtboard = selectedDocument.selectedLayers.layers[0]
  var pos_y=0, pos_x=0
  while(myArtboard){
    if(myArtboard.type == "Artboard"){
      break;
    }
    else if(myArtboard.parent){
      console.log(myArtboard.name + "selected")
      pos_y = myArtboard.frame.y + 100
      pos_x = myArtboard.frame.x
      myArtboard = myArtboard.parent
    }
    else {
      UI.message("Tex: Please select an artboard 🎨");
      return;
    }
  }
  console.log(pos_y, pos_x)
  const options = {
    identifier: webviewIdentifier,
    width: 400,
    height: 500,
    show: true,
  }
  const browserWindow = new BrowserWindow(options)

  // only show the window when the page has loaded to avoid a white flash
  browserWindow.once("ready-to-show", () => {
    browserWindow.show()
  })
  const webContents = browserWindow.webContents

  // print a message when the page loads
  webContents.on("did-finish-load", () => {
    UI.message("UI loaded!")
  })

  browserWindow.loadURL(require("../resources/webview.html"))
  
  browserWindow.webContents.on('svgUpdate', 
    function(svgString) {
      var group = sketch.createLayerFromData(svgString[0], "svg")
      var ngroup = group.layers[1]
      const tex_string = String(svgString[1])
      // console.log(ngroup.type)
      ngroup.parent = myArtboard      
      ngroup.name = "$" + tex_string
      // console.log(svgString[1])
      const ratio = 100/ngroup.frame.height
      const newFrame = new Rectangle(
        0, 0, ratio*ngroup.frame.width, ratio*ngroup.frame.height
      )
      ngroup.frame = newFrame
      ngroup.adjustToFit()
      // origin_eq.frame = newFrame
      ngroup.frame.x = pos_x
      ngroup.frame.y = pos_y
      const container = []
      for (var i = 0; i < tex_string.length; i++) {
        container.push(tex_string[i].charCodeAt(0));
      }
      Settings.setLayerSettingForKey(ngroup, 'tex-string', container)
      ngroup.selected = true
      UI.message("Tex: formula inserted to arttable " + myArtboard.name)
      console.log(Settings.layerSettingForKey(ngroup, 'tex-string'))
      browserWindow.close()
    })
}

// When the plugin is shutdown by Sketch (for example when the user disable the plugin)
// we need to close the webview if it's open
export function onShutdown() {
  const existingWebview = getWebview(webviewIdentifier)
  if (existingWebview) {
    existingWebview.close()
  }
}
