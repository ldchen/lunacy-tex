/* globals document */
import './app'

function setNativeValue(element, value) {
  const valueSetter = Object.getOwnPropertyDescriptor(element, 'value').set;
  const prototype = Object.getPrototypeOf(element);
  const prototypeValueSetter = Object.getOwnPropertyDescriptor(prototype, 'value').set;
  
  if (valueSetter && valueSetter !== prototypeValueSetter) {
  	prototypeValueSetter.call(element, value);
  } else {
    valueSetter.call(element, value);
  }
}

window.updatePreview = function (text) {
  const element = document.getElementById('eq-area')
  const ascii_array = text.split(",").map(Number);
  const reconstituted = String.fromCharCode.apply(null, ascii_array);
  setNativeValue(element, reconstituted);
  element.dispatchEvent(new Event('change', { bubbles: true }));
  document.getElementById('bt-submit').innerHTML = 'Update'
  document.getElementById('title').innerHTML = 'Editing...'
  console.log('Recived: ')
  console.log(text)
  console.log(reconstituted)
}; 


// Disable the context menu to have a more native feel
document.addEventListener('contextmenu', (e) => e.preventDefault())
